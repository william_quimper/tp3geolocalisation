package ca.cegepgarneau.graph_navigation_h2023

import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.view.Menu
import android.view.MenuItem
import android.widget.EditText
import android.widget.TextView
import com.google.android.material.snackbar.Snackbar
import com.google.android.material.navigation.NavigationView
import androidx.navigation.findNavController
import androidx.navigation.ui.AppBarConfiguration
import androidx.navigation.ui.navigateUp
import androidx.navigation.ui.setupActionBarWithNavController
import androidx.navigation.ui.setupWithNavController
import androidx.drawerlayout.widget.DrawerLayout
import androidx.appcompat.app.AppCompatActivity
import ca.cegepgarneau.graph_navigation_h2023.data.LocalisationDao
import ca.cegepgarneau.graph_navigation_h2023.data.UserDao
import ca.cegepgarneau.graph_navigation_h2023.data.dtb
import ca.cegepgarneau.graph_navigation_h2023.data.model.User
import ca.cegepgarneau.graph_navigation_h2023.databinding.ActivityMainBinding
import ca.cegepgarneau.graph_navigation_h2023.ui.home.HomeFragment
import ca.cegepgarneau.graph_navigation_h2023.ui.home.NameDialogFragment
import kotlin.concurrent.thread

class MainActivity : AppCompatActivity() {

    private lateinit var appBarConfiguration: AppBarConfiguration
    private lateinit var binding: ActivityMainBinding
    private lateinit var localisationDao: LocalisationDao
    private lateinit var userDao: UserDao
    private var _user: User? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        val mypreferences = MyPreferences(this)
        _user = mypreferences.getLoginAccount()

        localisationDao = dtb.getInstance(this).LocalisationDao()
        userDao = dtb.getInstance(this).UserDao()

        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        setSupportActionBar(binding.appBarMain.toolbar)

        val drawerLayout: DrawerLayout = binding.drawerLayout
        val navView: NavigationView = binding.navView
        val navController = findNavController(R.id.nav_host_fragment_content_main)
        // Passing each menu ID as a set of Ids because each
        // menu should be considered as top level destinations.
        appBarConfiguration = AppBarConfiguration(
            setOf(
                R.id.nav_home, R.id.nav_map
            ), drawerLayout
        )

        setupActionBarWithNavController(navController, appBarConfiguration)
        navView.setupWithNavController(navController)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId){
            R.id.action_config ->{
                if(_user != null){
                    val dialog = NameDialogFragment()
                    dialog.show(supportFragmentManager, "NameDialogFragment")
                }
                else{
                    val dialog = NameDialogFragment()
                    dialog.show(supportFragmentManager, "NameDialogFragment")
                }
                true
            }
            R.id.action_delete->{
                thread {
                    userDao.deleteAll()
                }
                Handler(Looper.getMainLooper()).post {
                    // Replace `HomeFragment` with the actual name of your fragment class
                    val homeFragment = supportFragmentManager.findFragmentByTag("HomeFragment") as? HomeFragment
                    homeFragment?.refresh()
                }
                //how to refresh homefragment
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        // Inflate the menu; this adds items to the action bar if it is present.
        menuInflater.inflate(R.menu.main, menu)
        return true
    }

    override fun onSupportNavigateUp(): Boolean {
        val navController = findNavController(R.id.nav_host_fragment_content_main)
        return navController.navigateUp(appBarConfiguration) || super.onSupportNavigateUp()
    }
}