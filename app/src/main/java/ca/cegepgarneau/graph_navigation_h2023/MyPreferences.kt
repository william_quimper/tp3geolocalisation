package ca.cegepgarneau.graph_navigation_h2023

import android.content.Context
import ca.cegepgarneau.graph_navigation_h2023.data.model.User

class MyPreferences(context:Context) {
    val PREFERENCE_NAME = "SharedPreferenceTP2"
    val PREFERENCE_UserN = "UserName"
    val PREFERENCE_UserLN = "UserLastName"
    val PREFERENCE_UserId = "UserId"

    val preference = context.getSharedPreferences(PREFERENCE_NAME, Context.MODE_PRIVATE)

    fun getLoginAccount(): User{
        val prenom = preference.getString(PREFERENCE_UserLN, "cegep").toString()
        val nom = preference.getString(PREFERENCE_UserN, "garneau").toString()
        val id = preference.getInt(PREFERENCE_UserId, 0)
        return User(id,nom,prenom)
    }

    fun setUser(user: User){
        val editor = preference.edit()
        editor.putString(PREFERENCE_UserLN, user.lastName)
        editor.putString(PREFERENCE_UserN, user.name)
        editor.putInt(PREFERENCE_UserId, user.id)
        editor.apply()
    }

}