package ca.cegepgarneau.graph_navigation_h2023.data

import androidx.lifecycle.LiveData
import androidx.room.*
import ca.cegepgarneau.graph_navigation_h2023.data.model.Localisation

@Dao
interface LocalisationDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertLocalisation(localisation: Localisation)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertAllLocalisation(localisation: List<Localisation>)

    @Update
    fun updateLocalisation(localisation: Localisation)

    @Query("DELETE FROM localisation_table")
    fun deleteAllLocalisation()

    @Query("SELECT * FROM localisation_table ")
    fun allLocalisation(): LiveData<List<Localisation>>
}