package ca.cegepgarneau.graph_navigation_h2023.data

import androidx.lifecycle.LiveData
import androidx.room.*
import ca.cegepgarneau.graph_navigation_h2023.data.model.User

@Dao
interface UserDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(user: User)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertAll(user: List<User>)

    @Query("SELECT * FROM user_table WHERE id = :id")
    fun getUser(id: Int): User?

    @Update
    fun update(user: User)

    @Query("DELETE FROM user_table")
    fun deleteAll()

    @Query("SELECT * FROM user_table ")
    fun allUser(): LiveData<List<User>>

}