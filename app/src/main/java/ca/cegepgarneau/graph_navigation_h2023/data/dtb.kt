package ca.cegepgarneau.graph_navigation_h2023.data

import android.content.Context
import android.util.Log
import androidx.room.*
import androidx.sqlite.db.SupportSQLiteDatabase
import ca.cegepgarneau.graph_navigation_h2023.data.model.Localisation
import ca.cegepgarneau.graph_navigation_h2023.data.model.User

/**
 * AppDatabase est une classe abstraite qui hérite de RoomDatabase
 * Elle contient une méthode abstraite qui retourne un objet de type LieuxDao
 * Room va générer le code nécessaire pour implémenter cette classe
 */
@Database(entities = [Localisation::class, User::class], version = 1, exportSchema = false)
abstract class dtb : RoomDatabase(){


    // Méthode abstraite qui retourne un objet de type LieuxDao
    abstract fun LocalisationDao(): LocalisationDao
    abstract fun UserDao(): UserDao

    // Singleton
    companion object{
        // @Volatile permet de garantir que les modifications sur cette variable
        // sont immédiatement visibles par tous les threads
        @Volatile
        // INSTANCE est un singleton, c'est-à-dire qu'il n'y a qu'une seule instance
        // de cette variable dans l'application
        private var INSTANCE: dtb? = null


        // Méthode qui retourne l'instance de la base de données
        // Si la base de données n'existe pas, elle est créée
        // Cette méthode est thread-safe, c'est-à-dire qu'elle peut être appelée
        // par plusieurs threads en même temps
        fun getInstance(context: Context): dtb {
            // Si l'instance n'existe pas, on la crée
            return INSTANCE ?: synchronized(this) {
                val instance = Room.databaseBuilder(
                    context,
                    dtb::class.java,
                    "database"
                )
                    // Pour ajouter des données à la base de données
                    // à la création de celle-ci
                    // (pour les tests)
                    .addCallback(object : Callback() {
                        override fun onCreate(db: SupportSQLiteDatabase) {
                            super.onCreate(db)

                            Log.d("TAG", "onCreate: BD")

                        }
                    })
                    .build()

                // On assigne l'instance à la variable INSTANCE
                INSTANCE = instance
                // On retourne l'instance
                instance
            }
        }

    }
}