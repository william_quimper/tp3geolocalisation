package ca.cegepgarneau.graph_navigation_h2023.data.model

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "localisation_table")
data class Localisation(
    @PrimaryKey(autoGenerate = true)
    val id: Int,
    @ColumnInfo(name = "info")
    var name: String,
    var message: String,
    var image: String,
    var longitude: Double,
    var latitude: Double
)