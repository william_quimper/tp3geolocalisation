package ca.cegepgarneau.graph_navigation_h2023.data.model

import android.graphics.BitmapFactory
import android.os.Handler
import android.os.Looper
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import ca.cegepgarneau.graph_navigation_h2023.R
import okhttp3.OkHttpClient
import okhttp3.Request
import org.json.JSONArray
import java.io.File
import java.io.InputStream
import kotlin.concurrent.thread

class LocalisationAdapter(private val lieux: List<Localisation>) :
    RecyclerView.Adapter<LocalisationAdapter.ViewHolder>(){

    // Interface pour gérer les clics sur les éléments de la liste
    interface OnItemClickListenerInterface {
        fun onItemClick(itemView: View?, position: Int)

    }

    // Objet qui implémente l'interface OnItemClickListener
    lateinit var listener: OnItemClickListenerInterface

    // Cette méthode permet de définir l'objet qui implémente l'interface OnItemClickListener
    // Elle est appelée dans la classe MainActivity
    fun setOnItemClickListener(listener: OnItemClickListenerInterface) {
        this.listener = listener
    }

    /**
     * Classe interne représentant les pointeurs vers les composants graphiques d'une ligne de la liste
     * Il y aura une instance de cette classe par ligne de la liste
     */
    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var tvName: TextView = itemView.findViewById(R.id.full_name)
        var tvMessage: TextView = itemView.findViewById(R.id.message_text)
        var imgPerson: ImageView = itemView.findViewById(R.id.profile_picture)

        // Constructeur
        init {
            // Ajoute un écouteur d'événement du click sur itemView (la ligne entière)
            itemView.setOnClickListener {
                // Récupère la position de l'élément cliqué
                val position = adapterPosition
                // Vérifie que la position est valide
                // (parfois, le clic est détecté alors que la position n'est pas encore déterminée)
                if (position != RecyclerView.NO_POSITION) {
                    // Appelle la méthode onItemClick de l'objet qui implémente l'interface OnItemClickListener
                    listener.onItemClick(itemView, position)

                }
            }
        }
    }


    // Cette méthode est appelée à chaque fois qu'il faut créer une ligne
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        // Utilise le layout person_one_line pour créer une vue
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.item_layout_liste, parent, false)

        // Crée un viewHolder en passant la vue en paramètre
        return ViewHolder(view)
    }

    // Cette méthode permet de lier les données à la vue
    // Elle remplit une ligne créée par onCreateViewHolder
    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        // Récupère l'élément de la liste à la position "position"
        val localisation: Localisation = lieux[position]
        // Met à jour les données de la vue
        holder.tvName.text = localisation.name
        holder.tvMessage.text = localisation.message

        thread{
            val path : String = localisation.name
            val client = OkHttpClient()
            val request = Request.Builder()
                .url("https://robohash.org/{${localisation.name.trim()}}")
                .build()
            val response = client.newCall(request).execute()
            if (response.isSuccessful) {
                val inputStream: InputStream? = response.body?.byteStream()
                val bitmap = BitmapFactory.decodeStream(inputStream)
                // Update the ImageView in the main thread using a Handler:
                Handler(Looper.getMainLooper()).post {
                    holder.imgPerson.setImageBitmap(bitmap)
                }
            }
        }
    }

    // Cette méthode permet de retourner le nombre d'éléments de la liste
    override fun getItemCount(): Int {
        return lieux.size
    }

}


