package ca.cegepgarneau.graph_navigation_h2023.data.model

data class Message(
    val id: Int,
    val author: String,
    val message: String,
    val lat: Double,
    val long: Double,
)