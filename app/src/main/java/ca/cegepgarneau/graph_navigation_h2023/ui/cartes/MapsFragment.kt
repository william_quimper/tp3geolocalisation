package ca.cegepgarneau.graph_navigation_h2023

import android.Manifest
import android.content.pm.PackageManager
import android.graphics.BitmapFactory
import android.location.Location
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import ca.cegepgarneau.graph_navigation_h2023.data.model.Localisation
import ca.cegepgarneau.graph_navigation_h2023.data.model.Message
import ca.cegepgarneau.graph_navigation_h2023.databinding.FragmentMapBinding
import ca.cegepgarneau.graph_navigation_h2023.ui.home.HomeViewModel
import com.google.android.gms.location.*
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.GoogleMap.*
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.Marker
import com.google.android.gms.maps.model.MarkerOptions
import okhttp3.OkHttpClient
import okhttp3.Request
import java.io.InputStream
import kotlin.concurrent.thread

class MapsFragment : Fragment(), OnMapReadyCallback, OnInfoWindowClickListener,
    InfoWindowAdapter, View.OnClickListener, OnMyLocationButtonClickListener,
    OnMyLocationClickListener {

    val LOCATION_PERMISSION_CODE = 1
    val TAG = "TAG"
    lateinit var mMap: GoogleMap
    private lateinit var btGo: Button
    private lateinit var btAdd: Button


    // pour enregistrer la position de l'utilisateur
    var userLocation: Location? = null

    private var markerCamera: Marker? = null

    private var messageList: MutableList<Localisation> = ArrayList()

    // pour suivre position de l'utilisateur
    private lateinit var fusedLocationClient: FusedLocationProviderClient

    // Déclaration pour le callback de la mise à jour de la position de l'utilisateur
    // Le callback est appelé à chaque fois que la position de l'utilisateur change
    private var locationCallback = object : LocationCallback() {
        override fun onLocationResult(locationResult: LocationResult) {
            super.onLocationResult(locationResult)
            userLocation = locationResult.lastLocation
            Log.d(TAG, "onLocationResult: ${userLocation?.latitude} ${userLocation?.longitude}")
        }
    }

    private lateinit var locationRequest: LocationRequest

    private var _binding: FragmentMapBinding? = null
    private val binding get() = _binding!!

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        // Service pour suivre la position de l'utilisateur
        // La position est déterminée à partir de plusieurs sources (GPS, réseau Wifi)
        fusedLocationClient = LocationServices.getFusedLocationProviderClient(requireActivity())

        _binding = FragmentMapBinding.inflate(inflater, container, false)

        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        val mapFragment = childFragmentManager
            .findFragmentById(R.id.map) as SupportMapFragment
        mapFragment.getMapAsync(this)



        btGo = binding.btGo
        btAdd = binding.btAdd
        btGo.setOnClickListener(this)
        btAdd.setOnClickListener(this)

        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     */
    override fun onMapReady(googleMap: GoogleMap) {
        // Initialisation de la carte
        mMap = googleMap
        // détection du click sur une fenêtre d'information d'un marqueur
        // (voir méthode onInfoWindowClick)
        mMap.setOnInfoWindowClickListener(this)
        // Permet de modifier l'apparence de la fenêtre d'information d'un marqueur
        // (voir méthode getInfoContents)
        mMap.setInfoWindowAdapter(this)

        // Permet de détecter le click sur le bouton de position de l'utilisateur
        // (voir méthode onMyLocationButtonClick)
        mMap.setOnMyLocationButtonClickListener(this)
        // Permet de détecter le click sur la position de l'utilisateur
        // (voir méthode onMyLocationClick)
        mMap.setOnMyLocationClickListener(this)


        // Détecter un click long sur la carte
        mMap.setOnMapLongClickListener { latLng ->
            Log.d(TAG, "onMapClick: $latLng")
            mMap.addMarker(MarkerOptions().position(latLng).title("Marker"))
            mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(latLng, 13f))
        }

        // Écouteur pour le drag&drop d'un marqueur
        mMap.setOnMarkerDragListener(object : OnMarkerDragListener {
            override fun onMarkerDragStart(marker: Marker) {
                Log.d(TAG, "onMarkerDragStart: " + marker.position)
            }

            override fun onMarkerDrag(marker: Marker) {
                Log.d(TAG, "onMarkerDrag: " + marker.position)
            }

            override fun onMarkerDragEnd(marker: Marker) {
                Log.d(TAG, "onMarkerDragEnd: " + marker.position)
            }
        })

        // Active la localisation de l'utilisateur
        enableMyLocation()


        // Vérifie les permissions avant d'utiliser le service fusedLocationClient.getLastLocation()
        // qui permet de connaître la dernière position
        if (ActivityCompat.checkSelfPermission(
                requireContext(), Manifest.permission.ACCESS_FINE_LOCATION
            ) != PackageManager.PERMISSION_GRANTED
            && ActivityCompat.checkSelfPermission(
                requireContext(), Manifest.permission.ACCESS_COARSE_LOCATION
            ) != PackageManager.PERMISSION_GRANTED
        ) {
            // Demande la permission à l'utilisateur
            ActivityCompat.requestPermissions(
                requireActivity(),
                arrayOf(Manifest.permission.ACCESS_FINE_LOCATION),
                LOCATION_PERMISSION_CODE
            )
            // Si la permission n'est pas accordée, on ne va pas plus loin
            return
        }


        // Récupère la dernière position connue
        fusedLocationClient.lastLocation
            .addOnSuccessListener(requireActivity()) { location ->
                // Vérifie que la position n'est pas null
                if (location != null) {
                    Log.d(TAG, "onSuccess: $location")
                    // Centre la carte sur la position de l'utilisateur au démarrage
                    val latLng = LatLng(location.latitude, location.longitude)
                    mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(latLng, 11f))
                }
            }

// Configuration pour mise à jour automatique de la position
        locationRequest = LocationRequest.Builder(Priority.PRIORITY_HIGH_ACCURACY, 10000L)
            .setWaitForAccurateLocation(false)
            .setMinUpdateIntervalMillis(10000L)
            .setMaxUpdateDelayMillis(10000L)
            .build()

// Création de la requête pour la mise à jour de la position
// avec la configuration précédente
        val request = LocationSettingsRequest.Builder()
            .addLocationRequest(locationRequest)
            .build()

// Création du client pour la mise à jour de la position.
// Le client va permettre de vérifier si la configuration est correcte,
// si l'utilisateur a activé ou désactivé la localisation
        val client = LocationServices.getSettingsClient(requireActivity())

// Vérifie que la configuration de la mise à jour de la position est correcte
// Si l'utilisateur a activé ou désactivé la localisation
        client.checkLocationSettings(request)
            .addOnSuccessListener {
                Log.d(TAG, "onSuccess: $it")
                // Si la configuration est correcte, on lance la mise à jour de la position
                fusedLocationClient.requestLocationUpdates(
                    // Configuration de la mise à jour de la position
                    locationRequest,
                    // Callback pour la mise à jour de la position
                    locationCallback,
                    null
                )
            }
            .addOnFailureListener {
                Log.d(TAG, "onFailure: $it")
                // Si la configuration n'est pas correcte, on affiche un message
                Toast.makeText(requireContext(), "Veuillez activer la localisation", Toast.LENGTH_SHORT).show()
            }

        messageList = ArrayList<Localisation>()
        val homeViewModel = ViewModelProvider(requireActivity())[HomeViewModel::class.java]

        val allLieux = homeViewModel.getAllLocalisation()
        allLieux.observe(viewLifecycleOwner){localisationLst ->
            messageList.clear()
            for (localisation in localisationLst){
                messageList.add(localisation)
            }
        }
        // Parcours la liste de messages et positionne les marqueurs
        // On met l'objet message dans le Tag du marqueur
        for (message in messageList) {
            val position = LatLng(message.latitude, message.longitude)
            mMap.addMarker(
                MarkerOptions()
                    .position(position)
                    .title("${message.name}")
            )?.tag = message
        }
    }
    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     */

    /**
     * Permet d'activer la localisation de l'utilisateur
     */
    private fun enableMyLocation() {
        // vérification si la permission de localisation est déjà donnée
        if (ContextCompat.checkSelfPermission(requireContext(), Manifest.permission.ACCESS_FINE_LOCATION)
            == PackageManager.PERMISSION_GRANTED
        ) {
            if (mMap != null) {
                // Permet d'afficher le bouton pour centrer la carte sur la position de l'utilisateur
                mMap.isMyLocationEnabled = true
            }
        } else {
            // La permission est manquante, demande donc la permission
            ActivityCompat.requestPermissions(
                requireActivity(),
                arrayOf(Manifest.permission.ACCESS_FINE_LOCATION),
                LOCATION_PERMISSION_CODE
            )
        }
    }

    /**
     * Gestion des permissions
     * Quand la permission a été demandée et que l'utilisateur a répondu
     * On vérifie s'il l'a accordée, ou s'il faut montrer la fénêtre d'explication
     *
     */
    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<String?>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        if (requestCode == LOCATION_PERMISSION_CODE) {
            if (grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                Toast.makeText(requireContext(), "Autorisation accordée", Toast.LENGTH_LONG).show()
                enableMyLocation()
            } else if (grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_DENIED) {
                if (ActivityCompat.shouldShowRequestPermissionRationale(
                        requireActivity(),
                        Manifest.permission.ACCESS_FINE_LOCATION
                    )
                ) {
                    val dialog = AlertDialog.Builder(requireContext())
                    dialog.setTitle("Permission requise !")
                    dialog.setMessage("Cette permission est importante pour la géolocalisation...")
                    dialog.setPositiveButton(
                        "Ok"
                    ) { dialog, which ->
                        requestPermissions(
                            arrayOf(
                                Manifest.permission.ACCESS_FINE_LOCATION
                            ), LOCATION_PERMISSION_CODE
                        )
                    }
                    dialog.setNegativeButton(
                        "Annuler"
                    ) { dialog, which ->
                        Toast.makeText(
                            requireContext(),
                            "Impossible de vous localiser",
                            Toast.LENGTH_SHORT
                        ).show()
                    }
                    dialog.show()
                }
            }
        }
    }

    /**
     * Gestion des clics sur les boutons
     */
    override fun onClick(view: View) {
        when (view.id) {
            R.id.bt_go -> {
                // Add a marker in Sydney and move the camera
                val sydney = LatLng(46.79, -71.26)
                mMap.addMarker(
                    MarkerOptions()
                        .position(sydney)
                        .title("Marker in Quebec")
                )
                // mMap.moveCamera(CameraUpdateFactory.newLatLng(sydney));
                mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(sydney, 13f))
            }
            R.id.bt_add -> {
                // positionner un marqueur au centre de la carte
                val cameraPosition = mMap.cameraPosition
                val position =
                    LatLng(cameraPosition.target.latitude, cameraPosition.target.longitude)
                markerCamera = mMap.addMarker(MarkerOptions().position(position).title("Titre"))
            }
        }
    }

    /**
     * Gestion des clics sur les fenêtres d'information des marqueurs
     */
    override fun onInfoWindowClick(marker: Marker) {
        val location = Location("Marker")
        location.latitude = marker.position.latitude
        location.longitude = marker.position.longitude

        // calculer la distance de l'utilisateur avec le marqueur sélectionné
        val distance = userLocation?.distanceTo(location)
        if (distance != null) {
            Toast.makeText(requireContext(), "Distance: ${distance / 1000} km", Toast.LENGTH_LONG).show()
        }
    }

    /**
     * Méthode pour modifier l'apparence d'une fenêtre d'information
     */
    override fun getInfoWindow(marker: Marker): View? {
        return null
    }

    /**
     * Méthode pour modifier le contenu d'une fenêtre d'information
     * en utilisant un layout
     */
    override fun getInfoContents(marker: Marker): View? {
        val view: View = LayoutInflater.from(requireContext()).inflate(R.layout.marker_layout, null)
        val tvAuthor = view.findViewById<TextView>(R.id.tv_author)
        val tvMessage = view.findViewById<TextView>(R.id.tv_message)
        val iv = view.findViewById<ImageView>(R.id.imageView)
        val latitude = marker.position.latitude
        tvAuthor.text = marker.title
        // On récupère le message qui est dans le Tag du marqueur
        val message: Message? = marker.tag as Message?
        tvMessage.text = if (message != null) message.message else null
        //        tvMessage.setText("lat:" + latitude);

        thread{
            val client = OkHttpClient()
            val request = Request.Builder()
                .url("https://robohash.org/hahaH")
                .build()
            val response = client.newCall(request).execute()
            if (response.isSuccessful) {
                val inputStream: InputStream? = response.body?.byteStream()
                val bitmap = BitmapFactory.decodeStream(inputStream)
                // Update the ImageView in the main thread using a Handler:
                iv.setImageBitmap(bitmap)
            }
        }

        return view
    }

    /**
     * Méthode pour détecter le click sur le bouton de position
     */
    override fun onMyLocationButtonClick(): Boolean {
        Log.d(TAG, "onMyLocationButtonClick: ")
        return false
    }

    /**
     * Méthode pour détecter le click sur la position de l'utilisateur
     */
    override fun onMyLocationClick(location: Location) {
        Log.d(TAG, "onMyLocationClick: $location")
    }
}


