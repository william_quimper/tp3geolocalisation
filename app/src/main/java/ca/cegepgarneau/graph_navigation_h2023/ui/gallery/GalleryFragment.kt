package ca.cegepgarneau.graph_navigation_h2023.ui.gallery

import android.content.Context
import android.content.ContextWrapper
import android.graphics.Bitmap
import android.graphics.ImageDecoder
import android.net.Uri
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.activity.result.ActivityResultLauncher
import androidx.activity.result.PickVisualMediaRequest
import androidx.activity.result.contract.ActivityResultContracts
import ca.cegepgarneau.graph_navigation_h2023.databinding.FragmentGalleryBinding
import java.io.File
import java.io.FileOutputStream
import java.io.IOException
import java.io.OutputStream
import java.util.*
import androidx.fragment.app.Fragment


class GalleryFragment : Fragment() {

    private var _binding: FragmentGalleryBinding? = null

    // pickMedia est un launcher qui permet de lancer une activité pour choisir une image
    private lateinit var pickMedia: ActivityResultLauncher<PickVisualMediaRequest>

    // savedImage est l'URI de l'image sauvegardée
    private var savedImage: Uri? = null


    private val binding get() = _binding!!

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {

        _binding = FragmentGalleryBinding.inflate(inflater, container, false)
        val root: View = binding.root

        return root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding.btImage.setOnClickListener {
            choosePhotoFromGallery()
        }

        // On enregistre le callback pour l'activité de sélection d'image
        pickMedia = registerForActivityResult(ActivityResultContracts.PickVisualMedia()) { uri ->
            // Le callback est appelé avec le résultat de l'activité
            if (uri != null) {
                binding.ivImage.setImageURI(uri)
                savedImage = saveImageToInternalStorage(uri)
                Log.d("PhotoPicker", "Selected URI: $uri")
            } else {
                Log.d("PhotoPicker", "No media selected")
            }
        }

    }

    /**
     * Cette méthode permet de sauvegarder une image dans le dossier interne de l'application
     * @param uri L'URI de l'image à sauvegarder
     * @return L'URI de l'image sauvegardée
     */
    private fun saveImageToInternalStorage(uri: Uri): Uri {
        val wrapper = ContextWrapper(context)
        var file = wrapper.getDir("TP2", Context.MODE_PRIVATE)
        file = File(file, "${UUID.randomUUID()}.jpg")
        // convert uri to bitmap
        val bitmap =
            ImageDecoder.decodeBitmap(ImageDecoder.createSource(context?.contentResolver!!, uri))
        try {
            val stream: OutputStream = FileOutputStream(file)
            bitmap.compress(Bitmap.CompressFormat.JPEG, 100, stream)
            stream.flush()
            stream.close()
        } catch (e: IOException) {
            e.printStackTrace()
        }
        return Uri.parse(file.absolutePath)
    }

    /**
     * Cette méthode permet de choisir une image dans la galerie
     */
    private fun choosePhotoFromGallery() {
        pickMedia.launch(PickVisualMediaRequest(ActivityResultContracts.PickVisualMedia.ImageOnly))
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}