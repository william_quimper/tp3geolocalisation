package ca.cegepgarneau.graph_navigation_h2023.ui.home

import android.os.AsyncTask
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import ca.cegepgarneau.graph_navigation_h2023.data.model.Localisation
import ca.cegepgarneau.graph_navigation_h2023.data.model.LocalisationAdapter
import ca.cegepgarneau.graph_navigation_h2023.data.model.User
import ca.cegepgarneau.graph_navigation_h2023.databinding.FragmentHomeBinding
import okhttp3.OkHttpClient
import okhttp3.Request
import org.json.JSONArray
import kotlin.concurrent.thread

class HomeFragment : Fragment() {

    private var _binding: FragmentHomeBinding? = null

    // This property is only valid between onCreateView and
    // onDestroyView.
    private val binding get() = _binding!!
    private lateinit var rvMessages: RecyclerView
    private lateinit var adapter: LocalisationAdapter
    private var mMessagesList: ArrayList<Localisation> = ArrayList()
    private lateinit var _user: User
    private lateinit var homeViewModel: HomeViewModel

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?
    ): View {
        homeViewModel = ViewModelProvider(this)[HomeViewModel::class.java]

        val dbLocalisation = homeViewModel.getAllLocalisation()
        if (dbLocalisation.value.isNullOrEmpty()) {
            thread {
                val client = OkHttpClient()
                val request = Request.Builder()
                    .url("https://kalanso.top/data.json")
                    .build()
                val response = client.newCall(request).execute()
                val json = JSONArray(response.body?.string())
                for (i in 0 until json.length()) {
                    val jsonObject = json.getJSONObject(i)
                    val latitude = jsonObject.getString("latitude")
                    val longitude = jsonObject.getString("longitude")
                    val firstname = jsonObject.getString("firstname")
                    val lastname = jsonObject.getString("lastname")
                    val message = jsonObject.getString("message")
                    val picture = jsonObject.getString("picture")
                    val name = "$firstname $lastname"
                    mMessagesList.add(
                        Localisation(
                            1,
                            name,
                            message,
                            picture,
                            latitude.toDouble(),
                            longitude.toDouble()
                        )
                    )
                }
                homeViewModel.insertAllLocalisation(mMessagesList)
                // Update the UI on the UI thread
                requireActivity().runOnUiThread {
                    adapter.notifyDataSetChanged()
                }
            }
        } else {
            for (i in 0..dbLocalisation.value!!.size) {
                mMessagesList.add(dbLocalisation.value!![i])
            }
        }

        _binding = FragmentHomeBinding.inflate(inflater, container, false)
        val root: View = binding.root

        return root
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        // Gestion du RecyclerView
        rvMessages = binding.rvMessages
        // fixe les dimensions du RecyclerView pour gain de performance
        rvMessages.setHasFixedSize(true)
        // Création de l'adapter
        adapter = LocalisationAdapter(mMessagesList)
        rvMessages.adapter = adapter

        val onItemClickListener: LocalisationAdapter.OnItemClickListenerInterface =
            object : LocalisationAdapter.OnItemClickListenerInterface {
                override fun onItemClick(itemView: View?, position: Int) {
                    Toast.makeText(
                        context, "Lieu Ajouté", Toast.LENGTH_SHORT
                    ).show()
                }
            }


        adapter.setOnItemClickListener(onItemClickListener)

        rvMessages.layoutManager = LinearLayoutManager(requireActivity())
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    fun refresh() {
        mMessagesList.clear()
        requireActivity().runOnUiThread {
            adapter.notifyDataSetChanged()
        }
    }
}