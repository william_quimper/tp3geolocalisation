package ca.cegepgarneau.graph_navigation_h2023.ui.home

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import ca.cegepgarneau.graph_navigation_h2023.data.LocalisationDao
import ca.cegepgarneau.graph_navigation_h2023.data.UserDao
import ca.cegepgarneau.graph_navigation_h2023.data.dtb
import ca.cegepgarneau.graph_navigation_h2023.data.model.Localisation
import ca.cegepgarneau.graph_navigation_h2023.data.model.User

class HomeViewModel(application: Application) : AndroidViewModel(application) {

    private val localisationDao : LocalisationDao = dtb.getInstance(application).LocalisationDao()
    private val userDao: UserDao = dtb.getInstance(application).UserDao()

    fun getAllLocalisation(): LiveData<List<Localisation>> {
        return localisationDao.allLocalisation()
    }
    fun deleteAllLocalisation() {
        localisationDao.deleteAllLocalisation()
    }
    fun insertLocalisation(lieu: Localisation) {
        localisationDao.insertLocalisation(lieu)
    }
    fun insertAllLocalisation(lieu: List<Localisation>) {
        localisationDao.insertAllLocalisation(lieu)
    }

    // User
    fun insertUser(user: User){
        userDao.insert(user)
    }

    fun getUser(id:Int){
        userDao.getUser(id)
    }

}