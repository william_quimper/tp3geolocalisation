package ca.cegepgarneau.graph_navigation_h2023.ui.home

import android.app.AlertDialog
import android.app.Dialog
import android.content.Context
import android.os.Bundle
import android.widget.EditText
import android.widget.Toast
import androidx.fragment.app.DialogFragment
import androidx.lifecycle.ViewModelProvider
import ca.cegepgarneau.graph_navigation_h2023.MyPreferences
import ca.cegepgarneau.graph_navigation_h2023.R
import ca.cegepgarneau.graph_navigation_h2023.data.model.User
import okhttp3.internal.notifyAll
import kotlin.concurrent.thread


class NameDialogFragment() : DialogFragment() {
    lateinit var user : User
    private lateinit var homeViewModel: HomeViewModel


    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        homeViewModel = ViewModelProvider(this)[HomeViewModel::class.java]
        return activity?.let {
            val builder = AlertDialog.Builder(it)
            val inflater = requireActivity().layoutInflater
            val view = inflater.inflate(R.layout.dialog_name, null)

            val mypreferences = MyPreferences(requireContext())
            user = mypreferences.getLoginAccount()

            if (user.name != null && user.lastName != null) {
                view.findViewById<EditText>(R.id.first_name)!!.setText(user.name)
                view.findViewById<EditText>(R.id.last_name)!!.setText(user.lastName)
            }
            builder.setView(view)
                .setPositiveButton("OK") { dialog, id ->
                // Handle positive button click here
                val firstName = view.findViewById<EditText>(R.id.first_name).text.toString()
                val lastName = view.findViewById<EditText>(R.id.last_name).text.toString()
                if (firstName.isNotEmpty() && lastName.isNotEmpty()){
                    thread{
                        homeViewModel.insertUser(user)
                    }
                    user.name = firstName
                    user.lastName = lastName
                    mypreferences.setUser(user)

                    dialog.dismiss()
                }
                else{
                    Toast.makeText(context, "Les informations entrés ne sont pas valides", Toast.LENGTH_SHORT).show()
                }
            }
                .setNegativeButton("Cancel") { dialog, id ->
                    // Handle negative button click here
                    dialog.cancel()
                }
            builder.create()
        } ?: throw IllegalStateException("Activity cannot be null")
    }
}